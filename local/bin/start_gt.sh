#!/bin/sh

export GT_PORT=${GT_PORT:-3346}

exec gt $([ ! -z "${GT_IP}" ] && echo "--ip ${GT_IP}" | sed -e 's/,/ --ip /g') $([ ! -z "${GT_PORT}" ] && echo "--port ${GT_PORT}") $([ ! -z "${GT_DNSTYPE}" ] && echo "--dnsType ${GT_DNSTYPE}") $([ ! -z "${GT_DNSSERVER}" ] && echo "--dnsServer ${GT_DNSSERVER}") $@

