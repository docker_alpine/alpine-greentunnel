# alpine-greentunnel
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-greentunnel)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-greentunnel)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-greentunnel/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-greentunnel/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [greentunnel](http://www.dest-unreach.org/greentunnel/)
    - Green Tunnel is an anti-censorship utility designed to bypass DPI system that are put in place by various ISPs to block access to certain websites.



----------------------------------------
#### Run

```sh
docker run -d \
           --net=host \
           -e GT_IP={GT_IP} \
           -e GT_PORT={GT_PORT} \
           -e GT_DNSTYPE={GT_DNSTYPE} \
           -e DT_DNSSERVER={GT_DNSSERVER} \
           forumi0721/alpine-greentunnel:[ARCH_TAG]
```



----------------------------------------
#### Usage

* Nothing to do



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| --net=host         | for external acess                               |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| GT_PORT            | HTTPS proxy port                                 |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| GT_IP              | Listening IP                                     |
| GT_PORT            | Listening Port                                   |
| GT_DNSTYPE         | DNS Type (DNS_OVER_HTTPS, DNS_OVER_TLS)          |
| GT_DNSSERVER       | DNS Server (https://cloudflare-dns.com/dns-query)|
| SOCKET_PATH        | Socket path                                      |
| SOCKET_ADDR        | Socket address                                   |

